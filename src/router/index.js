import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialCommunityIcons,FontAwesome } from 'react-native-vector-icons';
import { Login, Register, Home, DetailRecipe, Profil, Splash } from '../pages';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();


export default function Router() {
    return (
        <Stack.Navigator>
            <Stack.Screen component={Splash} name="Splash" options={{ headerShown: false }}/>
            <Stack.Screen component={Login} name="Login" options={{ headerShown: false }}/>
            <Stack.Screen component={Register} name="Register" options={{ headerShown: false }}/>
            <Stack.Screen component={Home} name="Home"  options={{ headerShown: false }}/>
            <Stack.Screen component={DetailRecipe} name="DetailRecipe"  options={{ headerShown: false }}/>
            <Stack.Screen component={MainApp} name="MainApp" options={{ headerShown: false }}  />
        </Stack.Navigator>
    )
}


const MainApp = () =>{ 
    return(
        <Tab.Navigator>
            <Tab.Screen component={Home} name="Home" 
            options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="home" color={color} size={size} />
            ),
        }}/>
        <Tab.Screen component={Profil} name="Profil" 
         options={{
            tabBarLabel: 'Profil',
            tabBarIcon: ({ color, size }) => (
                <FontAwesome name="user" color={color} size={size} />
            ),
        }}/>
        </Tab.Navigator>
    )
}
