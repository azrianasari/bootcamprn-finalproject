import { combineReducers } from "redux";

const initialStateApiKey = {
    ApiKey : "96306eac4e874169bdb0bfe3de64fb5d"
}

const APIReducer = (state = initialStateApiKey, action) => {
    return state;
};

const initialStatefirebase = {
    firebaseConfig : {
        apiKey: "AIzaSyCuHh0yzM4atuFgHJA4qB3WC-v-Y5Y6h90",
        authDomain: "recipesapp-26142.firebaseapp.com",
        projectId: "recipesapp-26142",
        storageBucket: "recipesapp-26142.appspot.com",
        messagingSenderId: "541693476712",
        appId: "1:541693476712:web:c074afe526186b3f33a0dd",
     },
}

const FirebaseReducer = (state = initialStatefirebase, action) => {
    return state;
};

const reducer = combineReducers({
    APIReducer,
    FirebaseReducer
});


export default reducer;