import React, {useEffect} from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'

export default function SplashScreen({navigation}) {
    useEffect(() => {
        setTimeout( () => {
            navigation.replace('Login');
        },3000)
    });
    return (
        <View style={styles.container}>
        <Image source={require('../assets/splash.png')}/>
            <Text style={{fontSize:17, fontWeight:'bold', color:'#2E3E5C', marginBottom:10, letterSpacing:0.4 }}>Start Cooking</Text>
            <Text style={{fontSize:12, fontWeight:'bold', color:'#9FA5C0', width:200, textAlign:'center', letterSpacing:0.2}} >Let’s join our community to cook better food!</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    } 
})
