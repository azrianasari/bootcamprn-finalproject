import React, {useState, useEffect, useRef} from 'react'
import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, FlatList, LogBox } from 'react-native'
import { FontAwesome , Ionicons, MaterialIcons } from '@expo/vector-icons'
import axios from 'axios'
import {useSelector} from 'react-redux'
import HTML from 'react-native-render-html'

export default function DetailRecipe({route , navigation}) {
    const { itemId } = route.params;
    const [items, setItems] = useState({});

    const APIReducer = useSelector((state) => state.APIReducer)

    const ApiKey = APIReducer.ApiKey


    const GetData = () => {
        axios.get(`https://api.spoonacular.com/recipes/${itemId}/information?apiKey=${ApiKey}&analyzedInstructions`)
        .then(result => {
            const data1 = (result.data)
            console.log('res : ', data1)
            setItems(data1)
        })
    }

    useEffect(() => {
        LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
        GetData()
        // console.log(htmlContent);
        
    }, [])

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Image source={{uri:items.image}} style={{height:300, width:380, position:'relative'}}/>
                <View style={{position:'absolute', backgroundColor:'#9FA5C0', top:50, left: 20, height:50, width:50, borderRadius:25, alignItems:'center',justifyContent:'center'}}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Ionicons name="arrow-back-circle-outline" size={30} color='white' />
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{backgroundColor:'white', borderTopRightRadius:24,borderTopLeftRadius:24, padding:16, justifyContent:'center',alignItems:'center'}}>
            <View style={{height:5, width:40, backgroundColor:'#979797', borderRadius:20, marginBottom:30}}/>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Text style={{color:'#3E5481', fontSize:17, fontWeight:'bold', marginBottom:20 }}>{items.title}</Text>

                <View style={{flexDirection:'row', alignItems:'center', marginBottom:10}}>
                    <Text style={{color:'#9FA5C0',fontSize:12, letterSpacing:0.2,marginRight:1}}>Food </Text>
                    <View style={{height:8,width:8, backgroundColor : '#9FA5C0', borderRadius:4, marginRight:5}}/>
                    <Text style={{color:'#9FA5C0',fontSize:12, letterSpacing:0.2,marginRight:1}}>{items.readyInMinutes} minutes</Text>
                </View>
                <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Image source={require('../assets/pict.png')}/>
                        <Text style={{color:'#3E5481',fontSize:14, marginLeft:14, letterSpacing:0.2, fontWeight:'bold'}}>{items.sourceName}</Text>      
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Image source={require('../assets/like.png')}/>
                        <Text style={{color:'#3E5481',fontSize:14, marginLeft:8, letterSpacing:0.2, fontWeight:'bold'}}>273 Likes</Text>      
                    </View>
                </View>

                <View style={styles.line}/>
            
                <Text style={{color:'#3E5481', fontSize:17, fontWeight:'bold', marginBottom:10}}>Description</Text>

                <HTML source={{ html: items.summary || '<p></p>' }} baseFontStyle = {{fontSize:13, color:'#3E5481', lineHeight:18}}/>

                <View style={styles.line}/>
                <Text style={{color:'#3E5481', fontSize:17, fontWeight:'bold', marginBottom:10}}>Ingredients</Text>

                <View >
                    <FlatList
                        horizontal={false}
                        numColumns={1}
                        showsVerticalScrollIndicator={false}
                        data={items.extendedIngredients}
                        keyExtractor = {(item, index) => `${item.id} - ${index}`}
                        renderItem = { ({item}) => {
                            return (
                                <View>
                                    <View style={{flexDirection:'row',alignItems:'center', marginTop:5}}>
                                        <Image source={require('../assets/CheckCircle.png')} style={{marginRight:10}}/>
                                        <Text style={{fontSize:13, color:'#3E5481', lineHeight:20, fontWeight:'normal'}}>{item.originalString}</Text>
                                    </View>

                                </View>
                            )
                        }
                    
                        }
                    />
                </View>

                <View style={styles.line}/>
                
                <Text style={{color:'#3E5481', fontSize:17, fontWeight:'bold', marginBottom:10}}>Steps</Text>
        
                <HTML source={{ html: items.instructions || '<p>No Steps</p>' }} 
                        baseFontStyle = {{fontSize:13, color:'#3E5481', lineHeight:18}}/>   
                              
                <Text style={{marginBottom:300}}></Text>        

            </ScrollView>
            </View>
           
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor:'white',
    },
    header:{
        paddingTop : 20,
        paddingBottom:10,
        height:300, 
        width:380,
        backgroundColor:'yellow'
    },
    line: {
        width:'100%',
        height:2,
        backgroundColor:'#F4F5F7',
        marginVertical:20
    }
})
