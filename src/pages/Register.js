import React, {useState, useEffect} from 'react'
import { Button, StyleSheet, Text, View, Image } from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import * as firebase from 'firebase';
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import {useSelector} from 'react-redux'

export default function Register({navigation}) {
    const FirebaseReducer = useSelector((state) => state.FirebaseReducer)

    const firebaseConfig = FirebaseReducer.firebaseConfig

     if(!firebase.apps.length){
        firebase.initializeApp(firebaseConfig);
     }
      const [email, setEmail] = useState("");
      const [password, setPassword] = useState("");

      const submit=()=>{
          const data = {
              email,
              password
          }
          console.log(data)
          firebase.auth().createUserWithEmailAndPassword(email, password).then(()=>{
            console.log('Register Berhasil');
              navigation.navigate("MainApp");
          }).catch(()=>{
            console.log("register gagal")
          })
      }
    return (
        
         <View style={styles.container}>
            <Text style={{fontSize:22, fontWeight:'bold', color:'#2E3E5C'}}>Welcome!</Text>
            <Text style={{fontSize:15, fontWeight:'normal', color:'#9FA5C0', marginTop:10}}>Please enter your account here</Text>
            <View style={{marginTop:50}}>
                <View style={{position:'relative'}}>
                    <TextInput 
                        style={styles.input}
                        placeholder="Email"
                        value={email}
                        onChangeText={(value)=>setEmail(value)}

                    />
                    <MaterialCommunityIcons name="email-outline" size={22} color="#3E5481" style={{position:'absolute', top:24, left:15}} />
                </View>
                <View style={{position:'relative'}}>
                    <TextInput 
                         style={styles.input}
                         placeholder="Password"
                         value={password}
                         onChangeText={(value)=>setPassword(value)}
                    />
                    <MaterialIcons name="lock-outline" size={24} color="#3E5481" style={{position:'absolute', top:22, left:15}} />
                </View>
                
                <View style={{marginTop:20}}>
                  <Text style={{color:'#3E5481', fontSize:12, marginBottom:5}}>Your Password must contain:</Text>
                    <View style={{flexDirection:'row'}}>
                        <Image source={require('../assets/CheckCircle.png')}/>
                        <Text style={{color:'#3E5481', fontSize:12, marginLeft:5}}>Atleast 6 characters</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Image source={require('../assets/CheckCircle.png')}/>
                        <Text style={{color:'#3E5481', fontSize:12, marginLeft:5}}>Contains a number</Text>
                    </View>
                </View>
                  
            </View>
            
            <TouchableOpacity onPress={submit} style={{marginVertical:20}}>
                <View style={{width:300, backgroundColor:'#1FCC79', height:45, borderRadius:32, alignItems:'center', justifyContent:'center'}}>
                    <Text style={{fontSize:14, fontWeight:'bold', color:'white'}}>Sign Up</Text>
                </View>
            </TouchableOpacity>

            <View style={{flexDirection:'row'}}>
                <Text style={{color:'#2E3E5C', fontSize:12}}>Already have login and password? </Text>
                <TouchableOpacity onPress={()=>navigation.navigate("Login")}>
                    <Text style={{fontSize:12, fontWeight:'bold', color: '#1FCC79'}}>Login</Text>
                </TouchableOpacity>
            </View>
            
        </View>
    )
}


const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems:'center',
        justifyContent:'center'
    },
    input:{
        borderWidth:1,
        borderColor:'#D0DBEA',
        paddingHorizontal:45,
        paddingVertical: 10,
        width: 300,
        marginBottom: 10,
        borderRadius: 32,
        marginTop: 10,
        color: '#9FA5C0'
    }
})
