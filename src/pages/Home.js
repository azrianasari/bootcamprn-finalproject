import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, FlatList ,Image } from 'react-native'
import axios from 'axios';
import {useSelector} from 'react-redux'

const ButtonCategory = (props) => {
    return (
        <View style={{ paddingVertical:10, paddingHorizontal:20, backgroundColor:'#1FCC79', borderRadius:32, marginRight:10}}>
            <Text style={{fontSize:13, fontWeight:'bold', color:'white'}}>{props.category}</Text>
        </View>
      
    )
   
}

export default function Home({navigation}) {
    
     const [items, setItems] = useState([]);
     const [search, setSearch] = useState('');

     const APIReducer = useSelector((state) => state.APIReducer)

     const ApiKey = APIReducer.ApiKey
   
     const GetData = () => {
         console.log(search)
         axios.get(`https://api.spoonacular.com/recipes/complexSearch?apiKey=${ApiKey}&addRecipeInformation=true&type=${search}`)
         .then(result => {
             const data1 = (result.data.results)
            //  console.log('res : ', data1)
             setItems(data1)
         })
     }
 
     const onSelectItem = (item) => {
         console.log(item.id)
            navigation.navigate('DetailRecipe', {
            itemId: item.id,
          });
 
     }

     const Search = (param) => {
        setSearch(param)
        GetData()      
     }
 
     
     useEffect(() => {
         GetData()
     }, [])
 
    return (
       
        <View style={styles.container}>
        <View style={styles.header}>
            <View style={{position:'relative'}}>
                <TextInput placeholder="Search" style={styles.search}/>
                <Image source={require('../assets/search.png')} style={{position:'absolute',top:13, left:15}}></Image>
            </View>
            <View>
                <Text style={{fontSize:14, fontWeight:'bold', color:'#3E5481', marginVertical:10}}>Category</Text>
                <View style={{flexDirection:'row'}}>
                    <TouchableOpacity onPress={() => Search('snack')}>
                        <ButtonCategory category='All' />    
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => Search('main cource')}>
                        <ButtonCategory category='Food'/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => Search('drink')}>
                        <ButtonCategory category='Drink'/>    
                    </TouchableOpacity>
                    
                </View>

            </View>
        </View>
        <View style={styles.line}/>

        <View style={styles.content}>

            <FlatList
                horizontal={false}
                numColumns={2}
                showsVerticalScrollIndicator={false}
                data={items}
                keyExtractor = {(item, index) => `${item.id} - ${index}`}
                renderItem = { ({item}) => {
                    return (
                        <View>
                            <TouchableOpacity onPress={()=>onSelectItem(item)} style={{padding: 3, marginBottom:10, marginRight:5}}>
                                <View style={{flexDirection:'row', alignItems:'center', marginBottom:10}}>
                                    <Image source={require('../assets/pict.png')}/>
                                    <Text style={{color:'#2E3E5C',fontSize:12, marginLeft:8, letterSpacing:0.2}}>{item.sourceName}</Text>      
                                </View>

                                <Image source={{uri:item.image}} style={{height:150, width:150,borderRadius: 15}} />
                                <View style={{width:150}}>
                                    <Text style={{color:'#3E5481',fontSize:14, fontWeight:'bold', marginVertical:10}}>{(item.title).substring(0,16)}</Text>
                                </View>

                                <View style={{flexDirection:'row', alignItems:'center'}}>
                                    <Text style={{color:'#9FA5C0',fontSize:12, letterSpacing:0.2,marginRight:1}}>Food </Text>
                                    <View style={{height:8,width:8, backgroundColor : '#9FA5C0', borderRadius:4, marginRight:5}}/>
                                    <Text style={{color:'#9FA5C0',fontSize:12, letterSpacing:0.2,marginRight:1}}>{item.readyInMinutes} minutes</Text>
                                </View>
                               
                            </TouchableOpacity>
                        </View>
                    )
                }

                }
            />
        </View>
    </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor:'white',
    },
    header:{
        paddingTop : 40,
        paddingHorizontal: 24,
        paddingBottom:10
    },
    search:{
        borderWidth: 1,
        paddingVertical : 10,
        paddingHorizontal: 45,
        borderRadius: 6,
        marginBottom: 5,
        borderRadius: 32,
        height:50,
        backgroundColor: '#F4F5F7',
        borderColor:'#E8E8E8',
        fontSize:13,
        fontWeight:'bold', 
        color:'#9FA5C0'

    },
    title: {
        color : 'white',
        fontSize: 20
    },
    content: {
        paddingHorizontal: 24,
        marginVertical:10,
        marginBottom:220
    },
    input:{
        borderWidth: 1,
        paddingVertical : 10,
        paddingHorizontal: 5,
        borderRadius: 6,
        marginBottom: 10
    },
    line: {
        height:6,
        width:'100%',
        backgroundColor:'#F4F5F7',
        marginVertical:10
    }
})

