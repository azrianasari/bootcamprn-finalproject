import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Button,FlatList ,Image } from 'react-native'
import * as firebase from 'firebase';
import axios from 'axios';
import { FontAwesome , Ionicons, MaterialIcons } from '@expo/vector-icons';
import {useSelector} from 'react-redux'


export default function Profil({navigation}) {

    const [user, setUser] = useState({})
    const [items, setItems] = useState([]);
    const [search, setSearch] = useState('Thai');

    const APIReducer = useSelector((state) => state.APIReducer)

    const ApiKey = APIReducer.ApiKey
 

    useEffect(() => {
        GetData()

        const userInfo =firebase.auth().currentUser
        setUser(userInfo)
    }, [])

    const onLogout=()=>{
        firebase.auth()
        .signOut()
            .then(()=>{
                console.log('user Sign out');
                navigation.navigate('Login')
            }
        )
        
    }
 
     const GetData = () => {
         console.log(search)
         axios.get(`https://api.spoonacular.com/recipes/complexSearch?apiKey=${ApiKey}&addRecipeInformation=true&cuisine=${search}`)
         .then(result => {
             const data1 = (result.data.results)
             console.log('res : ', data1)
             setItems(data1)
         })
     }
 
     const onSelectItem = (item) => {
         console.log(item.id)
            navigation.navigate('DetailRecipe', {
            itemId: item.id,
          });
 
     }

 
    return (
  
        <View style={styles.container}>
        <View style={styles.header}>
            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                <View>
                    <TouchableOpacity onPress={() => navigation.goBack()} style={{marginBottom:10}}>
                        <Ionicons name="arrow-back-circle-outline" size={30} color='#3E5481' />
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={onLogout}>
                        <MaterialIcons name="logout" size={27} color='#3E5481' />
                    </TouchableOpacity>
                </View>
            </View>
           
            <View style={{position:'relative', width:'100%', alignItems:'center'}}>
                <FontAwesome name="user-circle-o" size={100} color="black" />
                <Text style={{color:'#3E5481', fontSize:17, fontWeight:'bold', marginTop:10}}>{user.email}</Text>
            </View>
            <View>
                <View style={{flexDirection:'row', justifyContent:'space-around', marginTop:10}}>
                    <View style={{alignItems:'center'}}>
                        <Text style={{color:'#3E5481', fontSize:17, fontWeight:'bold'}}>32</Text>
                        <Text style={{color:'#9FA5C0', fontSize:12, fontWeight:'normal'}}>Recipes</Text>
                    </View>
                    <View style={{alignItems:'center'}}>
                        <Text style={{color:'#3E5481', fontSize:17, fontWeight:'bold'}}>782</Text>
                        <Text style={{color:'#9FA5C0', fontSize:12, fontWeight:'normal'}}>Following</Text>
                    </View>
                    <View style={{alignItems:'center'}}>
                        <Text style={{color:'#3E5481', fontSize:17, fontWeight:'bold'}}>1.287</Text>
                        <Text style={{color:'#9FA5C0', fontSize:12, fontWeight:'normal'}}>Followers</Text>
                    </View>
                </View>

            </View>
        </View>
        <View style={styles.line}/>

        <View style={styles.content}>
            <Text style={{fontSize:14, fontWeight:'bold', color:'#3E5481', marginBottom:10}}>Recipes</Text>
               
            <FlatList
                horizontal={false}
                numColumns={2}
                showsVerticalScrollIndicator={false}
                data={items}
                keyExtractor = {(item, index) => `${item.id} - ${index}`}
                renderItem = { ({item}) => {
                    return (
                        <View>
                            <TouchableOpacity onPress={()=>onSelectItem(item)} style={{padding: 3, marginBottom:10, marginRight:5}}>
                                <Image source={{uri:item.image}} style={{height:150, width:150,borderRadius: 15}} />
                                <View style={{width:150}}>
                                    <Text style={{color:'#3E5481',fontSize:14, fontWeight:'bold', marginVertical:10}}>{(item.title).substring(0,16)}</Text>
                                </View>

                                <View style={{flexDirection:'row', alignItems:'center'}}>
                                    <Text style={{color:'#9FA5C0',fontSize:12, letterSpacing:0.2,marginRight:1}}>Food </Text>
                                    <View style={{height:8,width:8, backgroundColor : '#9FA5C0', borderRadius:4, marginRight:5}}/>
                                    <Text style={{color:'#9FA5C0',fontSize:12, letterSpacing:0.2,marginRight:1}}>{item.readyInMinutes} minutes</Text>
                                </View>
                               
                            </TouchableOpacity>
                        </View>
                    )
                }

                }
            />
        </View>
    </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor:'white',
    },
    header:{
        paddingTop : 40,
        paddingHorizontal: 24,
        paddingBottom:10
    },
 
    title: {
        color : 'white',
        fontSize: 20
    },
    content: {
        paddingHorizontal: 24,
        marginVertical:10,
        marginBottom:220
    },
    input:{
        borderWidth: 1,
        paddingVertical : 10,
        paddingHorizontal: 5,
        borderRadius: 6,
        marginBottom: 10
    },
    line: {
        height:6,
        width:'100%',
        backgroundColor:'#F4F5F7',
        marginVertical:0
    }
})

