import Login from '../pages/Login';
import Register from '../pages/Register';
import Home from '../pages/Home';
import DetailRecipe from '../pages/DetailRecipe';
import Profil from '../pages/Profil';
import Splash from './Splash';


export {
    Login,
    Register,
    Home,
    DetailRecipe,
    Profil,
    Splash
}