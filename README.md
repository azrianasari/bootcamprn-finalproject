# Final Project
# Nama
Azriana Sari

# Nama Aplikasi
RecipesApp

# Mockup Figma
* [File-Figma](https://www.figma.com/file/7VkbOKCvT5DHYwbp9WgT3p/BootcampRN-FinalProject?node-id=156%3A0)

# Source Code
* [Link-Gitlab](https://gitlab.com/azrianasari/bootcamprn-finalproject)

# Demo Aplikasi
* [Link-Video](https://youtu.be/TWU7F-ZLuS4)

# Download Apk
* [Link-Download](https://expo.io/accounts/azrianasari/projects/RecipesApp)

# Sumber Referensi Template/Content
* [Desain-Figma](https://www.uistore.design/items/chefio-recipe-free-app-ui-kit-for-figma/)
* [Public-API](https://spoonacular.com/food-api/docs)
